﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TodoApi.Model
{
    public class TodoItem
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public bool IsComplete { get; set; }

        //private TodoItem() { }

        //public TodoItem(long id, string name)
        //{
        //    Id = id;
        //    Name = name;
        //    IsComplete = false;
        //}
    }
}
