﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TodoApi.Data;
using TodoApi.Model;

namespace TodoApi.Controllers
{
    [Route("Api/[controller]")]
    [ApiController]
    public class TodoController : ControllerBase
    {
        private readonly TodoContext context;

        public TodoController(TodoContext context)
        {
            this.context = context;

            if(!context.TodoItems.Any())
            {
                context.TodoItems.Add(new TodoItem() { Id = 1, Name = "Mi primer item" } );
                context.SaveChanges();
            }
        }

        //GET: api/Todo
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TodoItem>>> GetItems()
        {
            return await context.TodoItems.ToListAsync();
        }

        //GET: api/Todo/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TodoItem>> GetItem(long id)
        {
            var item = await context.TodoItems.FindAsync(id);

            if (item == null)
                return NotFound();

            return item;
        }

        //POST: api/Todo
        [HttpPost]
        public async Task<ActionResult<TodoItem>> PostItem(TodoItem item)
        {
            context.Add(item);
            await context.SaveChangesAsync();
            return CreatedAtAction(nameof(GetItem), new { id = item.Id }, item);
        }

        //PUT: api/Todo
        [HttpPut]
        public async Task<IActionResult> PutItem(long id, TodoItem item)
        {
            if (id != item.Id)
                return BadRequest();

            context.Entry(item).State = EntityState.Modified;
            await context.SaveChangesAsync();

            return NoContent();
        }

        //PATCH: api/Todo
        [HttpPatch]
        public async Task<IActionResult> PatchItem(long id, TodoItem item)
        {
            var databaseItem = context.TodoItems.Find(id);

            if (databaseItem == null)
                return BadRequest();

            databaseItem.Name = item.Name;
            databaseItem.IsComplete = item.IsComplete;
            context.Entry(databaseItem).State = EntityState.Modified;
            await context.SaveChangesAsync();
            return NoContent();
        }

        //DELETE: api/Todo/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteItem(long id)
        {
            var item = context.TodoItems.Find(id);

            if (item == null)
                return NotFound();

            context.TodoItems.Remove(item);
            await context.SaveChangesAsync();

            return NoContent();
        }
    }
}
