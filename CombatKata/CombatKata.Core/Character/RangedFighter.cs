﻿namespace CombatKata.Core
{
    public class RangedFighter : Player
    {
        public override Distance AtackRange => new Distance(20);
        public RangedFighter() : base() { }
    }
}
