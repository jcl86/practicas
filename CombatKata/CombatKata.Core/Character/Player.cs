﻿using System.Collections.Generic;
using System.Linq;

namespace CombatKata.Core
{
    public abstract class Player : Character
    {
        public abstract Distance AtackRange { get; }
        public List<Faction> Factions { get; }

        public Player()
        {
            Factions = new List<Faction>();
        }

        public void Damage(Player objetive, int damage, int distance)
        {
            if (AtackRange.IsInRange(distance))
            {
                if (!SharesFaction(objetive))
                    objetive.SufferDamage((int)(damage * DamageModifier(objetive)));
            }
        }

        public void Damage(BattleObject objetive, int damage, int distance)
        {
            if (AtackRange.IsInRange(distance))
            {
                objetive.SufferDamage(damage);
            }
        }

        private double DamageModifier(Character objetive)
        {
            if (objetive.Level >= Level + 5)
                return 0.5;

            if (objetive.Level + 5 <= Level)
                return 1.5;

            return 1;
        }

        public void Heal(Player objetive, int healthPoints)
        {
            if(SharesFaction(objetive))
                objetive.Heal(healthPoints);
        }

     

        public void Join(Faction faction)
        {
            if (!Factions.Contains(faction))
                Factions.Add(faction);
        }

        public void Leave(Faction faction)
        {
            Factions.Remove(faction);
        }

        private bool SharesFaction(Player otherPlayer)
        {
            return otherPlayer.Equals(this) ||
                Factions.Any(faction => otherPlayer.Factions.Contains(faction));
        }
    }
}
