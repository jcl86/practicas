﻿namespace CombatKata.Core
{
    public class MeleeFighter : Player
    {
        public override Distance AtackRange => new Distance(2);
        public MeleeFighter() : base() { }
    }
}
