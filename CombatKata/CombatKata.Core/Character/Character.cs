﻿using System;

namespace CombatKata.Core
{
    public class Character : HealthBase
    {
        public const int MaxHealth = 1000;

        public int Level { get; private set; }
        public void SetLevel(int level) => Level = level;

        public Character() : base(MaxHealth)
        {
            Level = 1;
        }

        public void Heal(int healthPoints)
        {
            if (!IsDead)
                Health = Math.Min(Health + healthPoints, MaxHealth);
        }

        public override string ToString() => $"Level: {Level} Health: {Health}/{MaxHealth}";
    }
}
