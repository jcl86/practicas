﻿using System;

namespace CombatKata.Core
{
    public abstract class HealthBase
    {
        private Guid id;

        public int Health { get; protected set; }

        public bool IsDead => Health <= 0;

        public HealthBase(int health)
        {
            id = Guid.NewGuid();
            Health = health;
        }

        public void SufferDamage(int damage)
        {
            Health = Math.Max(Health - damage, 0);
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;
            return ((HealthBase)obj).id.Equals(id);
        }

        public override int GetHashCode() => id.GetHashCode();
    }
}
