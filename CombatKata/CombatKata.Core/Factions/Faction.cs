﻿namespace CombatKata.Core
{
    public class Faction
    {
        private readonly string name;

        public Faction(string name)
        {
            this.name = name;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;
            return ((Faction)obj).ToString().Equals(name);
        }

        public override int GetHashCode() => name.GetHashCode();

        public override string ToString() => name;
    }


}
