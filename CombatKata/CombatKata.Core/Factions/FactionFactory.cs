﻿namespace CombatKata.Core
{
    public class FactionFactory
    {
        public static Faction Thiefs => new Faction("Thiefs");
        public static Faction Bards => new Faction("Bards");
        public static Faction Warriors => new Faction("Warriors");
    }
}
