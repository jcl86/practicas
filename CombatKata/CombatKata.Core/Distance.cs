﻿namespace CombatKata.Core
{
    public class Distance
    {
        public int Meters { get; }

        public Distance(int meters)
        {
            Meters = meters;
        }

        public bool IsInRange(int distance) => Meters >= distance;
    }
}
