﻿using CombatKata.Core;
using Xunit;

namespace CombatKata.Test
{
    public class RangeTests
    {
        [Fact]
        public void Should_make_damage_when_is_in_range()
        {
            //Arrange
            var player = new MeleeFighter();
            var enemy = new MeleeFighter();

            //Act
            player.Damage(enemy, 200, 2);

            //Assert
            Assert.Equal(800, enemy.Health);
        }

        [Fact]
        public void Should_not_make_damage_when_is_not_in_range()
        {
            //Arrange
            var player = new MeleeFighter();
            var enemy = new MeleeFighter();

            //Act
            player.Damage(enemy, 200, 3);

            //Assert
            Assert.Equal(1000, enemy.Health);
        }
    }
}
