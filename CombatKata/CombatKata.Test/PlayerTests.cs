﻿using CombatKata.Core;
using Xunit;

namespace CombatKata.Test
{
    public class PlayerTests
    {
        [Fact]
        public void Should_be_able_to_damage_enemies()
        {
            //Arrange
            var player = new RangedFighter();
            var enemy = new MeleeFighter();

            //Act
            player.Damage(enemy, 200, 1);

            //Assert
            Assert.Equal(800, enemy.Health);
        }

        [Fact]
        public void Should_not_be_able_to_damage_himself()
        {
            //Arrange
            var player = new RangedFighter();

            //Act
            player.Damage(player, 200, 1);

            //Assert
            Assert.Equal(Character.MaxHealth, player.Health);
        }

        [Fact]
        public void Should_be_able_to_heal_himself()
        {
            //Arrange
            var player = new RangedFighter();

            //Act
            player.SufferDamage(300);
            player.Heal(player, 200);

            //Assert
            Assert.Equal(900, player.Health);
        }

        [Fact]
        public void Should_not_be_able_to_heal_enemies()
        {
            //Arrange
            var player = new RangedFighter();
            var enemy = new RangedFighter();

            //Act
            player.Damage(enemy, 300, 1);
            player.Heal(enemy, 200);

            //Assert
            Assert.Equal(700, enemy.Health);
        }

        [Fact]
        public void Should_apply_boosted_damage_to_enemies_when_low_level()
        {
            //Arrange
            var player = new RangedFighter();
            player.SetLevel(6);
            var enemy = new RangedFighter();

            //Act
            player.Damage(enemy, 200, 1);

            //Assert
            Assert.Equal(700, enemy.Health);
        }

        [Fact]
        public void Should_apply_reduced_damage_to_enemies_when_high_level()
        {
            //Arrange
            var player = new RangedFighter();
            var enemy = new RangedFighter();
            enemy.SetLevel(6);

            //Act
            player.Damage(enemy, 200, 1);

            //Assert
            Assert.Equal(900, enemy.Health);
        }

        [Fact]
        public void Should_join_to_Faction()
        {
            //Arrange
            var player = new RangedFighter();
            var bardsFaction = FactionFactory.Bards;

            //Act
            player.Join(bardsFaction);

            //Assert
            Assert.Contains(bardsFaction, player.Factions);
        }

        [Fact]
        public void Should_leave_a_Faction()
        {
            //Arrange
            var player = new RangedFighter();
            var bardsFaction = FactionFactory.Bards;

            //Act
            player.Join(bardsFaction);
            player.Leave(bardsFaction);

            //Assert
            Assert.DoesNotContain(bardsFaction, player.Factions);
        }

        [Fact]
        public void Should_be_able_to_damage_things()
        {
            //Arrange
            var player = new RangedFighter();
            var house = ObjectFactory.House;

            //Act
            player.Damage(house, 200, 1);

            //Assert
            Assert.Equal(1800, house.Health);
        }
    }
}
