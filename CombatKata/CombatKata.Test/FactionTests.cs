﻿using CombatKata.Core;
using Xunit;

namespace CombatKata.Test
{
    public class FactionTests
    {
        [Fact]
        public void Should_not_damage_same_faction_players()
        {
            //Arrange
            var player = new RangedFighter();
            player.Join(FactionFactory.Thiefs);
            var ally = new MeleeFighter();
            ally.Join(FactionFactory.Thiefs);

            //Act
            player.Damage(ally, 200, 1);

            //Assert
            Assert.Equal(Character.MaxHealth, ally.Health);
        }

        [Fact]
        public void Should_damage_other_faction_players()
        {
            //Arrange
            var player = new RangedFighter();
            player.Join(FactionFactory.Thiefs);
            var enemy = new MeleeFighter();
            enemy.Join(FactionFactory.Warriors);

            //Act
            player.Damage(enemy, 200, 1);

            //Assert
            Assert.Equal(800, enemy.Health);
        }

        [Fact]
        public void Should_heal_same_faction_players()
        {
            //Arrange
            var player = new RangedFighter();
            player.Join(FactionFactory.Thiefs);
            var ally = new MeleeFighter();
            ally.Join(FactionFactory.Thiefs);

            //Act
            ally.SufferDamage(300);
            player.Heal(ally, 200);

            //Assert
            Assert.Equal(900, ally.Health);
        }
    }
}
