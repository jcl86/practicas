using CombatKata.Core;
using System;
using Xunit;

namespace CombatKata.Test
{
    public class CharacterTests
    {
        [Fact]
        public void Should_be_damaged_and_healed()
        {
            //Arrange
            var character = new Character();

            //Act
            character.SufferDamage(200);
            character.Heal(100);

            //Assert
            Assert.False(character.IsDead);
            Assert.Equal(900, character.Health);
        }

        [Fact]
        public void Should_die_when_health_below_0()
        {
            //Arrange
            var character = new Character();

            //Act
            character.SufferDamage(1200);

            //Assert
            Assert.True(character.IsDead);
            Assert.False(character.Health > 0);
        }

        [Fact]
        public void Should_not_be_healed_when_is_dead()
        {
            //Arrange
            var character = new Character();

            //Act
            character.SufferDamage(1200);
            character.Heal(100);

            //Assert
            Assert.True(character.IsDead);
            Assert.Equal(0, character.Health);
        }

        [Fact]
        public void Should_not_be_healed_up_to_1000()
        {
            //Arrange
            var character = new Character();

            //Act
            character.Heal(1000);

            //Assert
            Assert.True(character.Health == 1000);
        }
    }
}
