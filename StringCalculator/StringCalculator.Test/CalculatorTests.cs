using System;
using System.Collections.Generic;
using Xunit;

namespace StringCalculator.Test
{
    public class CalculatorTests
    {
        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("   ")]
        [InlineData("\n\n  ")]
        [InlineData("\n  ")]
        public void Should_return_0(string input)
        {
            var calculator = new Calculator();

            var result = calculator.Add(input);

            Assert.Equal(0, result);
        }

        [Theory]
        [InlineData("5")]
        [InlineData("2,3")]
        [InlineData("4,   1")]
        [InlineData("2\n3")]
        [InlineData("4\n   1")]
        public void Should_return_5(string input)
        {
            var calculator = new Calculator();

            var result = calculator.Add(input);

            Assert.Equal(5, result);
        }

        [Theory]
        [InlineData("1,  1,1,1,1,1,1")]
        [InlineData("2,3,2")]
        [InlineData("4,   1, 2")]
        [InlineData("4\n1, 2")]
        [InlineData("4,1\n2")]
        [InlineData("//;\n4;1;2")]
        [InlineData("//- \n 4- 1-  2")]
        public void Should_return_7(string input)
        {
            var calculator = new Calculator();

            var result = calculator.Add(input);

            Assert.Equal(7, result);
        }

        [Fact]
        public void Should_slice()
        {
            string input = "1,2,3\n4,5,6\n7";
            var slicer = new Slicer(new List<char>() { ',', '\n' });

            var result = slicer.Slice(input);

            var expected = new string[] { "1", "2", "3", "4", "5", "6", "7", };
            Assert.Equal(expected, result);
        }

        [Theory]
        [InlineData("2,  -1, -3")]
        [InlineData("2,-1,-3")]
        [InlineData("4,   -1, -3")]
        [InlineData("4\n-1, -3")]
        [InlineData("4,-1\n-3")]
        [InlineData("//;\n4;-1;-3")]
        [InlineData("//p \n 4p -1p  -3")]
        public void Should_not_allow_negatives(string input)
        {
            var calculator = new Calculator();

            var exception = Assert.Throws<NegativesNotAllowedException>(() => calculator.Add(input));
            Assert.Contains("-1", exception.Message);
            Assert.Contains("-3", exception.Message);
        }
    }
}
