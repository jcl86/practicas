﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace StringCalculator
{
    public class Parser
    {
        private const string DelimiterChanger = "//";
        private List<char> delimiters;

        public Parser()
        {
            delimiters = new List<char> { ',', '\n' };
        }

        public IEnumerable<int> Parse(string numbers)
        {
            if (numbers.StartsWith(DelimiterChanger))
            {
                delimiters = new List<char>() { numbers.Split('\n').First().Replace(DelimiterChanger, "").Trim().First() };
                numbers = numbers.Substring(numbers.IndexOf('\n'));
            }

            foreach (string number in new Slicer(delimiters).Slice(numbers))
                yield return int.Parse(number);
        }
    }
}
