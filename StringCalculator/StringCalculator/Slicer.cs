﻿using System.Collections.Generic;

namespace StringCalculator
{
    public class Slicer
    {
        private readonly List<char> delimiters;

        public Slicer(List<char> delimiters)
        {
            this.delimiters = delimiters;
        }

        public IEnumerable<string> Slice(string input)
        {
            return input.Split(delimiters.ToArray());
            //foreach (var delimiter in delimiters)
            //{
            //    if (input.Contains(delimiter))
            //    {

            //    }
            //    foreach (var part in input.Split(delimiter))
            //    {
            //        if (part.Contains(delimiter))
            //            GetParts(part);
            //        yield return int.Parse(number);
            //    }
            //}
        }
    }
}
