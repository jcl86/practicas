﻿using System;
using System.Collections.Generic;

namespace StringCalculator
{
    [Serializable]
    public class NegativesNotAllowedException : Exception
    {
        public NegativesNotAllowedException(IEnumerable<int> negativeNumbers) : base($"{string.Join(", ", negativeNumbers)} are negative not allowed numbers,") { }
        protected NegativesNotAllowedException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
