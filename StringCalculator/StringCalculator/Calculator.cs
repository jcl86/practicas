﻿using System.Linq;

namespace StringCalculator
{
    public class Calculator
    {
        public int Add(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
                return 0;

            var numbers = new Parser().Parse(input);
            if (numbers.Any(x => x < 0))
                throw new NegativesNotAllowedException(numbers.Where(x => x < 0));

            return numbers.Sum();
        }
    }
}
