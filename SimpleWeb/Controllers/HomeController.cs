﻿using Microsoft.AspNetCore.Mvc;
using SimpleWeb.Model;
using SimpleWeb.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleWeb.Controllers
{
    public class HomeController : Controller
    {
        private readonly IEmployeeRepository repository;

        public HomeController(IEmployeeRepository repository)
        {
            this.repository = repository;
        }

        public IActionResult Index()
        {
            var employees = repository.GetEmployees();
            return View(employees);
        }

        //public IActionResult Index(string search)
        //{
        //    var employees = repository.GetEmployees();
        //    return View(employees.Where(x => x.Name.Contains(search)).ToList());
        //}

        public IActionResult Details(int id)
        {
            var employee = repository.GetEmployee(id);

            if (employee == null)
                NotFound();

            var viewModel = new HomeDetailsViewModel()
            {
                Employee = employee,
                PageTitle = "Employee details"
            };
            return View(viewModel);
        }
    }
}
