﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleWeb.Model
{
    public class Employee
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Department { get; set; } 
    }
}
