﻿using SimpleWeb.Model;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleWeb.ViewModels
{
    public class HomeDetailsViewModel
    {
        public Employee Employee { get; set; }
        public string PageTitle { get; set; }
    }
}
