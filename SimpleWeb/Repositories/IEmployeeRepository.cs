﻿using System.Collections.Generic;

namespace SimpleWeb.Model
{
    public interface IEmployeeRepository
    {
        Employee GetEmployee(int id);
        IEnumerable<Employee> GetEmployees();
    }
}
