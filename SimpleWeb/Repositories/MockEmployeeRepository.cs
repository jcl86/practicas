﻿using System.Collections.Generic;
using System.Linq;

namespace SimpleWeb.Model
{
    public class MockEmployeeRepository : IEmployeeRepository
    {
        private List<Employee> employeeList;

        public MockEmployeeRepository()
        {
            employeeList = new List<Employee>()
            {
                new Employee()
                {
                    Id = 1,
                    Name = "Roberto",
                    Email = "roberto@empleado.com",
                    Department = "Compras"
                },
                 new Employee()
                {
                    Id = 2,
                    Name = "Lara",
                    Email = "lara@empleado.com",
                    Department = "Ventas"
                },
                 new Employee()
                {
                    Id = 3,
                    Name = "Julián",
                    Email = "jul@empleado.com",
                    Department = "Ventas"
                }
            };
        }

        public Employee GetEmployee(int id)
        {
            return employeeList.FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<Employee> GetEmployees() => employeeList.ToList();
    }
}
