﻿namespace Lonja
{
    public class Destino
    {
        public const int PorcDegradacionCada100km = 1;

        public static Destino Madrid => new Destino("Madrid", 800);
        public static Destino Barcelona => new Destino("Barcelona", 1100);
        public static Destino Lisboa => new Destino("Lisboa", 600);

        private readonly string nombre;
        private readonly int km;

        private Destino(string nombre, int km)
        {
            this.nombre = nombre;
            this.km = km;
        }

        public int CosteCarga() => 2 * km;
        public int PorcentajeDegradacion() => km / 100 * PorcDegradacionCada100km;
        public override string ToString() => nombre;

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            return ((Destino)obj).nombre.Equals(nombre);
        }

        public override int GetHashCode() => nombre.GetHashCode();
    }
}
