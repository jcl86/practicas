﻿namespace Lonja
{
    public class Pulpo : IMercancia
    {
        public int Cantidad { get; }

        public Pulpo(int cantidad)
        {
            Cantidad = cantidad;
        }

        public int PrecioMadrid => 0;
        public int PrecioBarcelona => 120;
        public int PrecioLisboa => 100;
    }
}