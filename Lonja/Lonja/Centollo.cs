﻿namespace Lonja
{
    public class Centollos : IMercancia
    {
        public int Cantidad { get; }

        public Centollos(int cantidad)
        {
            Cantidad = cantidad;
        }

        public int PrecioMadrid => 450;
        public int PrecioBarcelona => 0;
        public int PrecioLisboa => 500;
    }
}