﻿using System;

namespace Lonja
{
    public interface IMercancia
    {
        int Cantidad { get; }
        int PrecioMadrid { get; }
        int PrecioBarcelona { get; }
        int PrecioLisboa { get; }
    }
}