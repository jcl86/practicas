﻿namespace Lonja
{
    public class Vieiras : IMercancia
    {
        public int Cantidad { get; }

        public Vieiras(int cantidad)
        {
            Cantidad = cantidad;
        }

        public int PrecioMadrid => 500;
        public int PrecioBarcelona => 450;
        public int PrecioLisboa => 600;
    }
}