﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Lonja
{
    public class Carga
    {
        public const int MaximaCarga = 200;
        public int Gasto { get; }
        private List<IMercancia> mercancias = new List<IMercancia>();

        public Carga(params IMercancia[] mercancias)
        {
            this.mercancias = mercancias.ToList();

            if (mercancias.Sum(x => x.Cantidad) > 200)
                throw new ArgumentOutOfRangeException($"La carga admite como mucho {MaximaCarga} kg");

            Gasto = 5;
        }

        public int Precio { get; set; }

        public void TrasladarMadrid()
        {
            
        }
    }
}
