using Lonja;
using System;
using Xunit;

namespace LonjaTest
{
    public class LonjaTests
    {
        [Fact]
        public void Test1()
        {
            var vieiras = new Vieiras(50);
            var pulpo = new Pulpo(100);
            var centollos = new Centollos(50);
            var carga = new Carga(vieiras, pulpo, centollos);

            carga.Trasladar(Destino.Madrid);

            var beneficio = carga.Precio;
        }
    }
}
