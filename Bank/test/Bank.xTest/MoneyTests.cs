﻿using Xunit;
using Bank.Core;

namespace Bank.xTest
{
    public class MoneyTests
    {
        [Theory]
        [InlineData(0, "0,00")]
        [InlineData(-4500, "-45,00")]
        [InlineData(-45, "-0,45")]
        [InlineData(-03, "-0,03")]
        [InlineData(7, "0,07")]
        [InlineData(27, "0,27")]
        [InlineData(1659874536, "16598745,36")]
        [InlineData(-36, "-0,36")]
        [InlineData(100, "1,00")]
        [InlineData(12034, "120,34")]
        [InlineData(12003, "120,03")]
        [InlineData(-12003, "-120,03")]
        public void Should_store_money_values(int number, string expectedOutput)
        {
            //Arrange
            var money = new Money(number);

            //Act
            var result = money.ToString();

            //Assert
            Assert.Equal(expectedOutput, result);
        }

        [Theory]
        [InlineData(0, 34, "0,34")]
        [InlineData(-4500, 3124, "-13,76")]
        [InlineData(-45, 2700, "26,55")]
        [InlineData(-03,-6, "-0,09")]
        [InlineData(-03, 4, "0,01")]
        [InlineData(-15, 15, "0,00")]
        [InlineData(14, -15, "-0,01")]
        [InlineData(-14, 13, "-0,01")]
        [InlineData(7, 65, "0,72")]
        [InlineData(27, 90, "1,17")]
        [InlineData(1659874536, 200, "16598747,36")]
        [InlineData(-36, 600, "5,64")]
        [InlineData(100, 100, "2,00")]
        [InlineData(12034, -34, "120,00")]
        [InlineData(12003, -4, "119,99")]
        [InlineData(-12003, 3, "-120,00")]
        public void Should_add_money(int originalMoney, int moneyAdded, string expectedOutput)
        {
            //Arrange
            var money = new Money(originalMoney);

            //Act
            var result = money.AddMoney(new Money(moneyAdded)).ToString();

            //Assert
            Assert.Equal(expectedOutput, result);
        }

        [Theory]
        [InlineData(0, "0,00")]
        [InlineData(-4500, "45,00")]
        [InlineData(-45, "0,45")]
        [InlineData(-03, "0,03")]
        [InlineData(7, "0,07")]
        [InlineData(27, "0,27")]
        [InlineData(1659874536, "16598745,36")]
        [InlineData(-36, "0,36")]
        [InlineData(100, "1,00")]
        [InlineData(12034, "120,34")]
        [InlineData(12003, "120,03")]
        [InlineData(-12003, "120,03")]
        public void Should_get_AbsoluteValue(int number, string expectedOutput)
        {
            //Arrange
            var money = new Money(number);

            //Act
            var result = money.Abs().ToString();

            //Assert
            Assert.Equal(expectedOutput, result);
        }


    }
}
