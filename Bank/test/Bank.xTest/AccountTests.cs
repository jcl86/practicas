using System;
using Xunit;
using Bank.Core;
using Moq;

namespace Bank.xTest
{
    public class AccountTests
    {
        private readonly string[] expectedStatments = new string[] { "DATE || AMOUNT || BALANCE",
                                            "14/01/2012 || 500,00 || 1400,00",
                                            "12/01/2012 || -100,00 || 900,00",
                                            "10/01/2012 || 1000,00 || 1000,00" };

        [Fact]
        public void Should_print_Statements()
        {
            //Arrange
            var writer = new TestWriter();
            var date = new Mock<IDate>();
            var account = new Account(writer, date.Object);

            //Act
            date.Setup(x => x.GetDate).Returns(new DateTime(2012, 01, 10));
            account.Deposit(1000.ToMoney());

            date.Setup(x => x.GetDate).Returns(new DateTime(2012, 01, 12));
            account.Withdraw(100.ToMoney());

            date.Setup(x => x.GetDate).Returns(new DateTime(2012, 01, 14));
            account.Deposit(500.ToMoney());

            account.PrintStatements();

            //Assert
            Assert.Equal(expectedStatments, writer.Statements.ToArray());
        }

        [Fact]
        public void Should_Deposit_1000()
        {
            //Arrange
            var writer = new TestWriter();
            var date = new Mock<IDate>();
            var account = new Account(writer, date.Object);

            //Act
            date.Setup(x => x.GetDate).Returns(new DateTime(2012, 01, 10));
            account.Deposit(1000.ToMoney());
            account.PrintStatements();

            //Assert
            Assert.Contains("10/01/2012 || 1000,00 || 1000,00", writer.Statements);
        }

        [Fact]
        public void Should_Withdraw_100()
        {
            //Arrange
            var writer = new TestWriter();
            var date = new Mock<IDate>();
            var account = new Account(writer, date.Object);

            //Act
            date.Setup(x => x.GetDate).Returns(new DateTime(2012, 01, 12));
            account.Withdraw(100.ToMoney());
            account.PrintStatements();

            //Assert
            Assert.Contains("12/01/2012 || -100,00 || -100,00", writer.Statements);
        }

        [Fact]
        public void Should_Withdraw_100Negative()
        {
            //Arrange
            var writer = new TestWriter();
            var date = new Mock<IDate>();
            var account = new Account(writer, date.Object);

            //Act
            date.Setup(x => x.GetDate).Returns(new DateTime(2012, 01, 12));
            account.Withdraw((-100).ToMoney());
            account.PrintStatements();

            //Assert
            Assert.Contains("12/01/2012 || -100,00 || -100,00", writer.Statements);
        }

       
    }

    public class TransactionsTests
    {
        [Fact]
        public void Should_get_total_balance()
        {
            //Arrange
            var date = new Mock<IDate>();
            var transactionList = new TransactionList();
            date.Setup(x => x.GetDate).Returns(new DateTime(2012, 01, 10));

            //Act
            transactionList.AddPositiveTransaction(date.Object, 1000.ToMoney());
            transactionList.AddNegativeTransaction(date.Object, 100.ToMoney());
            transactionList.AddPositiveTransaction(date.Object, 500.ToMoney());

            //Assert
            Assert.Equal(1400.ToMoney(), transactionList.Balance);
        }
    }
}
