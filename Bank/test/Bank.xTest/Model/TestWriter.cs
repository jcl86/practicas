﻿using Bank.Core;
using System.Collections.Generic;

namespace Bank.xTest
{
    public class TestWriter : IWriter
    {
        public List<string> Statements;

        public TestWriter()
        {
            Statements = new List<string>();
        }

        public void Print(string text)
        {
            Statements.Add(text);
        }
    }
}
