namespace Bank.Core
{
    public interface IWriter
    {
        void Print(string text);
    }
}