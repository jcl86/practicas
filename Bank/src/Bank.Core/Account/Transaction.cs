﻿using System;

namespace Bank.Core
{
    public class Transaction
    {
        public DateTime Date { get; private set; }
        public Money Amount { get; private set; }

        public Transaction(DateTime date, Money amount)
        {
            Date = date;
            Amount = amount;
        }

        public override string ToString() => $"{Date.ToShortDateString()} || {Amount.ToString()}";
    }
}