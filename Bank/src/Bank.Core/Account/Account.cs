using System.Linq;

namespace Bank.Core
{

    public class Account
    {
        private IWriter writer;
        private readonly IDate date;
        private TransactionList transactions;

        public Account(IWriter writer, IDate date)
        {
            this.writer = writer;
            this.date = date;
            transactions = new TransactionList();
        }

        public void Deposit(Money amount)
        {
            transactions.AddPositiveTransaction(date, amount);
        }

        public void Withdraw(Money amount)
        {
            transactions.AddNegativeTransaction(date, amount);
        }

        public void PrintStatements()
        {
            writer.Print(StatementsHeader);
            foreach (var statementLine in transactions.GetOrderedStatements)
                writer.Print(statementLine.ToString());
        }

        private string StatementsHeader => "DATE || AMOUNT || BALANCE";
    }
}