﻿namespace Bank.Core
{
    public class StatementLine
    {
        private readonly Money balance;
        public Transaction Transaction { get; private set; }

        public StatementLine(Transaction transaction, Money balance)
        {
            Transaction = transaction;
            this.balance = balance;
        }

        public override string ToString() => $"{Transaction.ToString()} || {balance}";
    }
}