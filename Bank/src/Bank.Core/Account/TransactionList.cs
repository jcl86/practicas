﻿using System.Collections.Generic;

namespace Bank.Core
{
    public class TransactionList
    {
        private List<Transaction> transactions;

        public TransactionList()
        {
            transactions = new List<Transaction>();
        }

        public void AddPositiveTransaction(IDate date, Money amount) => transactions.Add(new Transaction(date.GetDate, amount));
        public void AddNegativeTransaction(IDate date, Money amount)
        {
            var negativeAmount = new Money(amount.Abs().Amount * (-1));
            transactions.Add(new Transaction(date.GetDate, negativeAmount));
        }

        public IEnumerable<StatementLine> GetOrderedStatements => GetStatements.OrderByDescending(x => x.Transaction.Date);
        private IEnumerable<StatementLine> GetStatements
        {
            get
            {
                var balance = new Money(0);
                foreach (var transaction in transactions.OrderBy(x => x.Date))
                {
                    balance = balance.AddMoney(transaction.Amount);
                    yield return new StatementLine(transaction, balance);
                }
            }
        }

        public Money Balance
        {
            get
            {
                var total = new Money(0);
                foreach (var transaction in transactions)
                    total = total.AddMoney(transaction.Amount);
                return total;
            }
        }
    }
}