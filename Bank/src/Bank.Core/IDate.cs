﻿using System;

namespace Bank.Core
{
    public interface IDate
    {
        DateTime GetDate { get; }
    }
}