using System;

namespace Bank.Core
{
    public class Money
    {
        public int Amount { get; }

        public char Sign => IsPositive ? '+' : '-';
        public bool IsPositive => Amount >= 0;
        public bool IsNegative => !IsPositive;
        public Money Abs() => new Money(Math.Abs(Amount));

        public Money(int moneyAmount)
        {
            this.Amount = moneyAmount;
        }

        public Money AddMoney(Money money)
        {
            return new Money(money.Amount + Amount);
        }

        public override bool Equals(object obj)
        {
            return obj is Money money &&
                   Amount == money.Amount;
        }

        public override int GetHashCode() => Amount.GetHashCode();

        public override string ToString()
        {
            int entire = Math.Abs(Amount / 100);
            int cents = Math.Abs(Amount % 100);
            string centsText = (cents.ToString().Length == 1) ? $"0{cents}" : cents.ToString();
            return $"{(IsNegative ? Sign.ToString() : "")}{entire},{centsText}";
        }
    }
}