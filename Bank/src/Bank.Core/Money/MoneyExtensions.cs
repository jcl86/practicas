﻿namespace Bank.Core
{
    public static class MoneyExtensions
    {
        public static Money ToMoney(this int number)
        {
            return new Money(number * 100);
        }
    }
}