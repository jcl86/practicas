namespace Bank.Core
{
    public class Euros : Money
    {
        public Euros(int moneyAmount) : base(moneyAmount)
        {
        }
        public override string ToString() => base.ToString() + "€";
    }
}