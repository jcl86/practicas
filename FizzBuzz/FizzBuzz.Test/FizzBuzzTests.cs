using System;
using System.Collections.Generic;
using Xunit;

namespace FizzBuzz.Test
{

    public class FizzBuzzTests
    {
        private class TestLogger : ILogger
        {
            private List<string> entries;

            public TestLogger()
            {
                entries = new List<string>();
            }

            public void Info(string message) => entries.Add(message);
            public string GetEntry(int i) => entries[i - 1];
        }

        [Fact]
        public void Should_print_Fizz_Buzz_in_first_30_numbers()
        {
            var log = new TestLogger();
            var fizzBuzz = new FizzBuzzCalculator(log);

            fizzBuzz.Print();

            Assert.Equal("1", log.GetEntry(1));
            Assert.Equal("2", log.GetEntry(2));
            Assert.Equal("Fizz", log.GetEntry(3));
            Assert.Equal("4", log.GetEntry(4));
            Assert.Equal("Buzz", log.GetEntry(5));
            Assert.Equal("Fizz", log.GetEntry(6));
            Assert.Equal("7", log.GetEntry(7));
            Assert.Equal("8", log.GetEntry(8));
            Assert.Equal("Fizz", log.GetEntry(9));
            Assert.Equal("Buzz", log.GetEntry(10));
            Assert.Equal("11", log.GetEntry(11));
            Assert.Equal("Fizz", log.GetEntry(12));
            Assert.Equal("13", log.GetEntry(13));
            Assert.Equal("14", log.GetEntry(14));
            Assert.Equal("FizzBuzz", log.GetEntry(15));
            Assert.Equal("16", log.GetEntry(16));
            Assert.Equal("17", log.GetEntry(17));
            Assert.Equal("Fizz", log.GetEntry(18));
            Assert.Equal("19", log.GetEntry(19));
            Assert.Equal("Buzz", log.GetEntry(20));
            Assert.Equal("Fizz", log.GetEntry(21));
            Assert.Equal("22", log.GetEntry(22));
            Assert.Equal("23", log.GetEntry(23));
            Assert.Equal("Fizz", log.GetEntry(24));
            Assert.Equal("Buzz", log.GetEntry(25));
            Assert.Equal("26", log.GetEntry(26));
            Assert.Equal("Fizz", log.GetEntry(27));
            Assert.Equal("28", log.GetEntry(28));
            Assert.Equal("29", log.GetEntry(29));
            Assert.Equal("FizzBuzz", log.GetEntry(30));
        }
    }
}
