﻿namespace FizzBuzz
{
    public interface ILogger
    {
        void Info(string message);
    }
}