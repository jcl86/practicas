﻿using System;

namespace FizzBuzz
{
    public class ConsoleLogger : ILogger
    {
        public void Info(string message) => Console.WriteLine(message);
    }
}
