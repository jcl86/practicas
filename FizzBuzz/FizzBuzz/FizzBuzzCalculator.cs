﻿using System.Linq;

namespace FizzBuzz
{
    public class FizzBuzzCalculator
    {
        private readonly ILogger log;

        public FizzBuzzCalculator(ILogger log)
        {
            this.log = log;
        }

        public void Print()
        {
            var results = Enumerable.Range(1, 100).Select(number => new FizzBuzzer(number).ToString());
            foreach (var entry in results)
                log.Info(entry);
        }
    }
}
