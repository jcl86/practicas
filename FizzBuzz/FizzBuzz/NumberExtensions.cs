﻿namespace FizzBuzz
{
    public static class NumberExtensions
    {
        public static bool CanBeDividedBy3(this int number) => number % 3 == 0;
        public static bool CanBeDividedBy5(this int number) => number % 5 == 0;
    }
}
