﻿using System;

namespace FizzBuzz
{
    class Program
    {
        static void Main(string[] args)
        {
            new FizzBuzzCalculator(new ConsoleLogger()).Print();
            Console.ReadKey();
        }
    }
}
