﻿namespace FizzBuzz
{
    public class FizzBuzzer
    {
        private readonly int number;

        public FizzBuzzer(int number)
        {
            this.number = number;
        }

        public override string ToString()
        {
            if (number.CanBeDividedBy3() && number.CanBeDividedBy5()) return "FizzBuzz";
            if (number.CanBeDividedBy3()) return "Fizz";
            if (number.CanBeDividedBy5()) return "Buzz";
            return number.ToString();
        }
    }
}
